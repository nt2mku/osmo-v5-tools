#!/bin/bash
declare -A PSTNPORTS
declare -A ISDNPORTS
declare -A OSMOV5CFG
declare -A OSMOPSTNCFG
declare -A OSMOISDNCFG

CONFGEN_DIR="/etc/osmocom/confgen"
OSMO_V5_CFGFILE="$CONFGEN_DIR/osmo-v5-le.cfg"
OSMO_V5_TPLFILE="/etc/osmocom/templates/osmo-v5-le.tpl"
OSMO_PSTN_CFGFILEDIR="$CONFGEN_DIR"
OSMO_PSTN_TPLFILE="/etc/osmocom/templates/osmo-cc-pstn-endpoint.tpl"
OSMO_ISDN_CFGFILEDIR="$CONFGEN_DIR"
OSMO_ISDN_TPLFILE="/etc/osmocom/templates/osmo-cc-misdn-endpoint.tpl"

# V5 Interface address of AN
V5_IFADDR="8198"
# Local system IP address
SYSIP="192.168.32.70"
LCLIP="127.0.0.1"

# V5 AN PSTN port range or ISDN EF address range
PSTNPORTS[0]="300 331"
#PSTNPORTS[1]="500 531"
ISDNPORTS[0]="700 715"
#ISDNPORTS[1]="900 915"

OSMOV5CFG['v5_if']="$V5_IFADDR"

OSMOPSTNCFG['cc_ip']="$SYSIP"
OSMOPSTNCFG['lcl_ip']="$LCLIP"
OSMOPSTNCFG['v5_if']="$V5_IFADDR"
OSMOPSTNCFG['verboselevel']="1"
OSMOPSTNCFG['metering']="metering"
OSMOPSTNCFG['recall']=""
OSMOPSTNCFG['loop_disconnect']=""
OSMOPSTNCFG['lr_on_connect']=""
OSMOPSTNCFG['cc_ringback']="cc-ringback"
OSMOPSTNCFG['local_tones']="german"

OSMOISDNCFG['cc_ip']="$SYSIP"
OSMOISDNCFG['lcl_ip']="$LCLIP"
OSMOISDNCFG['v5_if']="$V5_IFADDR"
OSMOISDNCFG['verboselevel']="1"
OSMOISDNCFG['aocd']="aocd"
OSMOISDNCFG['aocs']="aocs"
OSMOISDNCFG['cnip']="cnip"
OSMOISDNCFG['cc_ringback']="cc-ringback"
OSMOISDNCFG['layer_1_hold']="layer-1-hold 1"
OSMOISDNCFG['local_tones']="german"

template() {
    ARRAY_NAME="$1"
    TEMPLATE_F="$2"
    OUTPUT_F="$3"

    local -n ARRAY="$ARRAY_NAME"
    for key in ${!ARRAY[@]}; do
        export $key="${ARRAY[$key]}"
    done

    cat "$TEMPLATE_F" | envsubst "$(printf '$%s ' "${!ARRAY[@]}")" > "$OUTPUT_F"
}

echo -e "This script will restart V5-LE and all endpoints! Continue? "
read ok


### Stop and disable PSTN endpoints
#cd /etc/systemd/system/multi-user.target.wants
#for i in `ls osmo-cc-pstn-endpoint@*`; do
#	echo "Stop and disable endpoint $i..."
#	systemctl stop $i
#	systemctl disable $i
#done
#for i in `ls osmo-cc-misdn-endpoint@*`; do
#	echo "Stop and disable endpoint $i..."
#	systemctl stop $i
#	systemctl disable $i
#done
echo "Stop and disable PSTN/ISDN endpoints..."
systemctl stop osmo-cc-pstn-endpoint@\*.service
systemctl disable osmo-cc-pstn-endpoint@.service
systemctl stop osmo-cc-misdn-endpoint@\*.service
systemctl disable osmo-cc-misdn-endpoint@.service

### Stop OSMO-V5-LE

echo "Stopping V5-LE..."
systemctl stop osmo-v5-le.service
echo "Stopping E1D..."
systemctl stop osmo-e1d.service

### Remove all config files from CONFGEN directory

rm $CONFGEN_DIR/*.cfg

### Generate osmo-v5-le.cfg

for portgroup in ${!PSTNPORTS[@]}; do
	for i in `seq ${PSTNPORTS[$portgroup]}`; do
		OSMOV5CFG['pstnports']+="$(printf %b "port pstn $i\n  line-echo-suppressor\n ")"
	done
done
for portgroup in ${!ISDNPORTS[@]}; do
	for i in `seq ${ISDNPORTS[$portgroup]}`; do
		OSMOV5CFG['isdnports']+="$(printf %b "port isdn $i\n ")"
	done
done

template "OSMOV5CFG" $OSMO_V5_TPLFILE $OSMO_V5_CFGFILE

### Start OSMO-V5-LE

echo "Starting E1D..."
systemctl start osmo-e1d.service
echo "Starting V5-LE..."
systemctl start osmo-v5-le.service

### Generate osmo-cc-pstn-endpoint-xxx.cfg

for portgroup in ${!PSTNPORTS[@]}; do
	for i in `seq ${PSTNPORTS[$portgroup]}`; do
		OSMOPSTNCFG['v5_port']="$i"
		OSMOPSTNCFG['cc_port']="4$(printf "%03d" $i)"
		template "OSMOPSTNCFG" $OSMO_PSTN_TPLFILE $OSMO_PSTN_CFGFILEDIR/osmo-cc-pstn-endpoint-${V5_IFADDR}_$i.cfg
	done
done

### Generate osmo-cc-misdn-endpoint-xxx.cfg

for portgroup in ${!ISDNPORTS[@]}; do
	for i in `seq ${ISDNPORTS[$portgroup]}`; do
		OSMOISDNCFG['v5_port']="$i"
		OSMOISDNCFG['cc_port']="4$(printf "%03d" $i)"
		template "OSMOISDNCFG" $OSMO_ISDN_TPLFILE $OSMO_ISDN_CFGFILEDIR/osmo-cc-misdn-endpoint-${V5_IFADDR}_$i.cfg
	done
done

### Enable and start endpoints

for portgroup in ${!PSTNPORTS[@]}; do
	for i in `seq ${PSTNPORTS[$portgroup]}`; do
		echo "Enable PSTN endpoint ${V5_IFADDR}_$i"
		systemctl --no-reload enable osmo-cc-pstn-endpoint@${V5_IFADDR}_$i.service
	done
done

echo "Reloading systemd"
systemctl daemon-reload

for portgroup in ${!PSTNPORTS[@]}; do
	for i in `seq ${PSTNPORTS[$portgroup]}`; do
		echo "Start PSTN endpoint ${V5_IFADDR}_$i"
		systemctl start osmo-cc-pstn-endpoint@${V5_IFADDR}_$i.service
	done
done

for portgroup in ${!ISDNPORTS[@]}; do
	for i in `seq ${ISDNPORTS[$portgroup]}`; do
		echo "Enable ISDN endpoint ${V5_IFADDR}_$i"
		systemctl --no-reload enable osmo-cc-misdn-endpoint@${V5_IFADDR}_$i.service
	done
done

echo "Reloading systemd"
systemctl daemon-reload

for portgroup in ${!ISDNPORTS[@]}; do
	for i in `seq ${ISDNPORTS[$portgroup]}`; do
		echo "Start ISDN endpoint ${V5_IFADDR}_$i"
		systemctl start osmo-cc-misdn-endpoint@${V5_IFADDR}_$i.service
	done
done
