#!/bin/bash
declare -A PSTNPORTS
declare -A ISDNPORTS

PSTNPORTS[0]="300 331"
#PSTNPORTS[1]="500 531"
ISDNPORTS[0]="500 515"

V5_IFADDR=8198

### Enable and start endpoints

if [ "$1" = "pstn" ]; then
	for portgroup in ${!PSTNPORTS[@]}; do
		for i in `seq ${PSTNPORTS[$portgroup]}`; do
			echo "Restart PSTN endpoint ${V5_IFADDR}_$i"
			systemctl restart osmo-cc-pstn-endpoint@${V5_IFADDR}_$i.service
		done
	done
elif [ "$1" = "isdn" ]; then
	for portgroup in ${!ISDNPORTS[@]}; do
		for i in `seq ${ISDNPORTS[$portgroup]}`; do
			echo "Restart ISDN endpoint ${V5_IFADDR}_$i"
			systemctl restart osmo-cc-misdn-endpoint@${V5_IFADDR}_$i.service
		done
	done
fi

