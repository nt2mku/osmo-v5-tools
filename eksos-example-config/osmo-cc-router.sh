#!/bin/bash

# Disable expansion to process digit '*'
set -f

fn_autodial()
{
	# Automatic dialing
	AUTODIALTIME=0
	#[ "$V5IF" = "8198" ] && [ "$V5PORT" = "500" ] && [ "$V5SN" = "20" ] && AUTODIAL="0311" AUTODIALTIME=1
	#[ "$V5IF" = "8198" ] && [ "$V5PORT" = "316" ] && AUTODIAL="01191" AUTODIALTIME=1

	if [ "$AUTODIAL" != "" ]; then
		#[ "$AUTODIALTIME" -gt 0 ] && echo "alerting" && sleep $AUTODIALTIME
		[ "$AUTODIALTIME" -gt 0 ] && sleep $AUTODIALTIME
		CC_DIALING="$AUTODIAL"
		CC_COMPLETE=1
	fi
}

fn_redialcallback()
{
	# Redial, callback and shortcut dial
	[ -d /tmp/osmo-cc ] || mkdir /tmp/osmo-cc

	# DTMF key "B" -> Redial
	if [ "$CC_DIALING" = "B" ]; then
		CC_DIALING=""
		[ -e /tmp/osmo-cc/redial_${V5IF}-${V5PORT} ] && CC_DIALING=`cat /tmp/osmo-cc/redial_${V5IF}-${V5PORT}` CC_COMPLETE=1
		[ -e /tmp/osmo-cc/redial_${V5IF}-${V5PORT}-${V5SN} ] && CC_DIALING=`cat /tmp/osmo-cc/redial_${V5IF}-${V5PORT}-${V5SN}` CC_COMPLETE=1
	# DTMF keys "AA" -> Call back last calling number
	elif [ "$CC_DIALING" = "AA" ]; then
		CC_DIALING=""
		[ -e /tmp/osmo-cc/callback_${V5IF}-${V5PORT} ] && CC_DIALING=`cat /tmp/osmo-cc/callback_${V5IF}-${V5PORT} CC_COMPLETE=1`
		[ -e /tmp/osmo-cc/callback_${V5IF}-${V5PORT}-${V5SN} ] && CC_DIALING=`cat /tmp/osmo-cc/callback_${V5IF}-${V5PORT}-${V5SN}` CC_COMPLETE=1
	# DTMF key "D" + 1 digit -> Shortcut dialing
	elif [ "${CC_DIALING:0:1}" = "D" ]; then
		SHORTDIAL=${CC_DIALING:1}
		CC_DIALING=""
		[ -e /etc/osmocom/shortdials/shortdial_${V5IF}-${V5PORT}_${SHORTDIAL} ] && CC_DIALING=`cat /etc/osmocom/shortdials/shortdial_${V5IF}-${V5PORT}_${SHORTDIAL}` CC_COMPLETE=1
		[ -e /etc/osmocom/shortdials/shortdial_${V5IF}-${V5PORT}-${V5SN}_${SHORTDIAL} ] && CC_DIALING=`cat /etc/osmocom/shortdials/shortdial_${V5IF}-${V5PORT}-${V5SN}_${SHORTDIAL}` CC_COMPLETE=1
	fi

	# Update redial
	if [ "$CC_DIALING" != "" ]; then
		[ "$V5SN" != "" ] && echo "$CC_DIALING" > /tmp/osmo-cc/redial_${V5IF}-${V5PORT}-${V5SN}
		[ "$V5SN" = "" ] && echo "$CC_DIALING" > /tmp/osmo-cc/redial_${V5IF}-${V5PORT}
	fi
}

fn_registercallback()
{
	# Register calling number for callback feature
	[ -d /tmp/osmo-cc ] || mkdir /tmp/osmo-cc
	if [ "$CC_CALLING" != "" ]; then
		[ "$V5SN" != "" ] && echo "$CC_CALLING" > /tmp/osmo-cc/callback_${V5IF}-${V5PORT}-${V5SN}
		[ "$V5SN" = "" ] && echo "$CC_CALLING" > /tmp/osmo-cc/callback_${V5IF}-${V5PORT}
	else
		[ "$V5SN" != "" ] && rm /tmp/osmo-cc/callback_${V5IF}-${V5PORT}-${V5SN}
		[ "$V5SN" = "" ] && rm /tmp/osmo-cc/callback_${V5IF}-${V5PORT}
	fi
}

fn_addcplnum()
{
	# Add complete numbers to the fast overlap dial directory
	[ "$1" = "" ] && return
	[ ! -e /etc/osmocom/phonebook2.txt ] && return
	grep -qxFf /etc/osmocom/phonebook2.txt <<< "$1" && return
	echo "$1" >> /etc/osmocom/phonebook2.txt
}

fn_phonebook()
{
	# Check for complete numbers from phone book, param 1 = number
	# Phonebook file contains one number per line, e.g. 0311
	[ -e /etc/osmocom/phonebook.txt ] && grep -qxFf /etc/osmocom/phonebook.txt <<< "$1" && return 0
	[ -e /etc/osmocom/phonebook2.txt ] && grep -qxFf /etc/osmocom/phonebook2.txt <<< "$1" && return 0
	# LDAP query
	#/etc/osmocom/ldap-phonequery.sh "$1" && return 0
	return 1
}

fn_blocknumbers()
{
	# Number blocking, param 1 = number
	[[ "$1" =~ ^01[0-4,8-9] ]] 	&& return 0
	[[ "$1" =~ ^090[0-5] ]]		&& return 0
	[[ "$1" =~ ^11[8-9] ]]		&& return 0
	return 1
}

fn_overlapdial()
{
	# Overlap dialing
	echo "overlap"
	# Timeout for first digit = 60 secs
	DIAL_TO=60
	# Else timeout = 4 secs
	[ "$CC_DIALING" != "" ] && DIAL_TO=4

	while true; do
		# Overlap dial complete handling here
		DIALNUM=$CC_DIALING
		[[ "$DIALNUM" =~ ^0049 ]]			&& DIALNUM="0${DIALNUM:4}"
		[[ "$DIALNUM" =~ ^0421[1-9] ]]			&& DIALNUM="${DIALNUM:4}"
		[[ "$DIALNUM" =~ ^AA ]]				&& break
		[[ "$DIALNUM" =~ ^B ]]				&& break
		[[ "$DIALNUM" =~ ^D[0-9] ]]			&& break
		[[ "$DIALNUM" =~ ^\#[3][0-9]{2}$ ]]		&& break
		[[ "$DIALNUM" =~ ^\#[5][0-9]{4}$ ]]		&& break
		[[ "$DIALNUM" =~ ^11[0,2]$ ]]			&& break
		[[ "$DIALNUM" =~ ^01191$ ]]			&& break
		[[ "$DIALNUM" =~ ^01910$ ]]			&& break
		[[ "$DIALNUM" =~ ^031[0-1]$ ]]			&& break
		# Phonebook entries
		fn_phonebook "$DIALNUM"				&& break

		# Not complete, read next digit
		read -t $DIAL_TO msg
		[ $? -gt 128 ] && break

		msg_type=${msg%%\ *}	msg=${msg:${#msg_type}+1}
		msg_digit=${msg%%\ *}	msg=${msg:${#msg_digit}+1}

		[ "$msg_type" != "dialing" ] && echo "disconnect" && exit 0

		if [ "$msg_digit" = "P" ]; then
			# Payphone coin tone
			COINS=$(($COINS + 1))
		else
			# Concat dialed digits
			CC_DIALING="$CC_DIALING$msg_digit"
			# Timeout for further digits = 4 secs
			DIAL_TO=4
		fi
	done
}

fn_checkcoins()
{
	# At least 2 coins?
	if [ $COINS -lt 2 ]; then
		# Not enough coins
		echo "rtp-proxy orig-codecs=PCMA,telephone-event term-codecs=PCMA,telephone-event"
		#echo "tx-gain gain=-10"
		#echo "rx-gain gain=-10"
		echo "proceeding"
		echo "play filename=/etc/osmocom/sounds/no-coins.wav"
		echo "disconnect"
		exit 0
	fi
}

fn_metering()
{
	# Determine charging information
	SUBSCR_TYPE="Customer"

	# Payphone ports
	[[ "$V5PORT" =~ ^30[0,2,3,5,7]$ ]]	&& SUBSCR_TYPE="Payphone"
	[[ "$V5PORT" =~ ^50[2]$ ]]		&& SUBSCR_TYPE="Payphone"

	if [ "$SUBSCR_TYPE" = "Payphone" ] ; then
		# Payphones
		# Default values for local calls
		CHRG_SEC=27
		CHRG_C_UNITS=1

		# National
		[[ "$CC_DIALING" =~ ^0 ]]			&& CHRG_SEC=22
		# Special
		[[ "$CC_DIALING" =~ ^011 ]]			&& CHRG_SEC=15
		# Mobile
		[[ "$CC_DIALING" =~ ^01[5-7] ]]			&& CHRG_SEC=20
		# Test/Freecall
		[[ "$CC_DIALING" =~ ^031[0-1] ]] 		&& CHRG_SEC=0		CHRG_C_UNITS=0
		[[ "$CC_DIALING" =~ ^0800 ]]			&& CHRG_SEC=0		CHRG_C_UNITS=0
		# Regional
		[[ "$CC_DIALING" =~ ^04 ]]			&& CHRG_SEC=25
		# International
		[[ "$CC_DIALING" =~ ^00 ]]			&& CHRG_SEC=10
		# National
		[[ "$CC_DIALING" =~ ^0049 ]]			&& CHRG_SEC=22
		# Emergency
		[[ "$CC_DIALING" =~ ^11[0,2] ]]			&& CHRG_SEC=0 		CHRG_C_UNITS=0

		# Functions
		[[ "$CC_DIALING" =~ ^\* ]]			&& CHRG_SEC=0		CHRG_C_UNITS=0
		[[ "$CC_DIALING" =~ ^\# ]]			&& CHRG_SEC=0		CHRG_C_UNITS=0
	
	else
		# Non-payphones
		CHRG_SEC=0
		CHRG_C_UNITS=0

		[[ "$CC_DIALING" =~ ^00 ]]			&& CHRG_SEC=30	 	CHRG_C_UNITS=1
		[[ "$CC_DIALING" =~ ^011 ]]			&& CHRG_SEC=15	 	CHRG_C_UNITS=1
	fi

	echo "metering connect_units=$CHRG_C_UNITS period=$CHRG_SEC"
}

# Determine calling interface type
CALLINGNETTYPE="unknown"
[ "$CC_NETWORK_TYPE" = "2" ] && CALLINGNETTYPE="POTS"
[ "$CC_NETWORK_TYPE" = "3" ] && CALLINGNETTYPE="ISDN"
[ "$CC_NETWORK_TYPE" = "4" ] && CALLINGNETTYPE="SIP"

# Calls from SIP to AN
if [ "$CC_CALLING_INTERFACE" = "v5-sip" ] || [ "$CC_CALLING_INTERFACE" = "sip" ] ; then

	V5INFO=$CC_DIALING
	# V5 Access Network Interface
	V5IF=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5IF}+1}
	# V5 Access Port / EFAddr
	V5PORT=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5PORT}+1}
	# Subscriber no (e.g. MSN)
	V5SN=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5SN}+1}

	[ "$V5PORT" = "" ] && V5PORT="$V5IF" V5IF=8198

	fn_registercallback

	echo "rtp-proxy orig-codecs=PCMA,telephone-event term-codecs=PCMA,telephone-event"

	#echo "tx-gain gain=-10"
	#echo "rx-gain gain=-10"

	[ "$V5SN" = "" ] && echo "call interface=v5-$V5IF-$V5PORT sending-complete"
	[ "$V5SN" != "" ] && echo "call interface=v5-$V5IF-$V5PORT dialing=$V5SN sending-complete"
	
	read -t 15 callprogress
	#[ $? -gt 128 ] && echo "disconnect isdn_cause=41"
	while true; do
		[ "$callprogress" = "call-answer" ] && echo "called-dtmf"
		[ "$callprogress" = "call-proceeding" ] && true
		[ "$callprogress" = "call-disconnect" ] && true
		if [[ "$callprogress" =~ ^dtmf ]]; then
			dtmfdigit="${callprogress:5}"
			[[ "$dtmfdigit" = "*" ]] && dtmfdigit="star"
			[[ "$dtmfdigit" = "#" ]] && dtmfdigit="pound"
			echo "called-play filename=/etc/osmocom/sounds/dtmf-${dtmfdigit}.wav"
		fi
		[[ "$callprogress" =~ ^called-dtmf ]] && echo "telephone-event event=${callprogress:12}"
		read callprogress
	done

# Calls from AN to AN/SIP
else

	V5INFO=$CC_CALLING_INTERFACE
	# v5-8198-500 or 8198-isdn-500
	V5MARK=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5MARK}+1}
	# Bug with ISDN subscribers, interface name wrong
	if [ "$V5MARK" != "v5" ]; then
		# V5 Access Network Interface
		V5IF=$V5MARK
		# (Discard isdn/pstn)
		V5DISCARD=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5DISCARD}+1}
		# V5 Access Port / EFAddr
		V5PORT=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5PORT}+1}
	else
		# V5 Access Network Interface
		V5IF=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5IF}+1}
		# V5 Access Port / EFAddr
		V5PORT=${V5INFO%%-*}	V5INFO=${V5INFO:${#V5PORT}+1}
	fi

	# Subscriber no (e.g. MSN)
	V5SN=$CC_CALLING

	COINS=0

	# Automatic dialing
	fn_autodial

	# Collect digits (overlap dialing)
	if [ "$CC_DIALING" = "" ] || [ "$CC_COMPLETE" = "0" ]; then
		fn_overlapdial
	fi

	# DTMF "AA" - Callback, "B" - Redial, "D" - Shortdial
	fn_redialcallback

	# Dialing timeout without digits
	[ "$CC_DIALING" = "" ] && echo "disconnect" && exit 0
	DIALNUM="$CC_DIALING"

	# Check inserted coins
	#fn_checkcoins

	# Pre-Metering Aliases
	[[ "$CC_DIALING" =~ ^0049 ]]			&& CC_DIALING="0${CC_DIALING:4}"
	[[ "$CC_DIALING" =~ ^0421[1-9] ]]		&& CC_DIALING="${CC_DIALING:4}"

	# Determine metering information
	fn_metering

	# Block certain numbers
	fn_blocknumbers "$CC_DIALING"			&& DIALOUT="DISC"

	# Internal connections
	[[ "$CC_DIALING" =~ ^\#[3][0-9][0-9] ]]		&& DIALOUT="LOCAL" 	UIF="8198"	UPORT="${CC_DIALING:1}"		UMSN=""
	[[ "$CC_DIALING" =~ ^\#[5][0-9][0-9] ]]		&& DIALOUT="LOCAL"	UIF="8198"	UPORT="${CC_DIALING:1:3}"	UMSN="${CC_DIALING:4}"

	if [ "$DIALOUT" = "LOCAL" ]; then
		# Dialout via Intern

		if [ "$CC_CALLING_PRESENT" = "1" ]; then
			[ "$UMSN" = "" ] && echo "call interface=v5-$UIF-$UPORT sending-complete"
			[ "$UMSN" != "" ] && echo "call interface=v5-$UIF-$UPORT dialing=$UMSN sending-complete"
		else
			[ "$UMSN" = "" ] && echo "call interface=v5-$UIF-$UPORT calling=#$V5PORT$CC_CALLING sending-complete"
			[ "$UMSN" != "" ] && echo "call interface=v5-$UIF-$UPORT dialing=$UMSN calling=#$V5PORT$CC_CALLING sending-complete"
		fi
		read -t 8 callprogress
		#[ $? -gt 128 ] && echo "disconnect isdn_cause=41" && exit 0
		while true; do
			[ "$callprogress" = "call-answer" ] && true
			[ "$callprogress" = "call-proceeding" ] && true
			[ "$callprogress" = "call-disconnect" ] && true
			read callprogress
		done

	elif [ "$DIALOUT" = "DISC" ]; then
		# Reject call
		echo "disconnect"
		exit 0
	else
		# Dialout via SIP
		# Reject data calls
		if [ "$CC_BEARER_CAPABILITY_NAME" != "speech" ] && [ "$CC_BEARER_CAPABILITY_NAME" != "audio" ] && [ "$CC_BEARER_CAPABILITY_NAME" != "" ]; then
			echo "disconnect"
			exit 0
		fi

		echo "rtp-proxy orig-codecs=PCMA,telephone-event term-codecs=PCMA,telephone-event"
		
		#echo "tx-gain gain=-0"
		#echo "rx-gain gain=-10"

		echo "call interface=v5-sip dialing=$CC_DIALING calling=$V5IF-$V5PORT-$CC_CALLING sending-complete"

		read -t 8 callprogress
		#[ $? -gt 128 ] && echo "disconnect isdn_cause=41" && exit 0
		while true; do
			[ "$callprogress" = "call-answer" ] && echo "dtmf" && fn_addcplnum "$DIALNUM"
			[ "$callprogress" = "call-proceeding" ] && true
			[ "$callprogress" = "call-alerting" ] && true 
			[ "$callprogress" = "call-disconnect" ] && true
			[[ "$callprogress" =~ ^dtmf ]] && echo "called-telephone-event event=${callprogress:5}"
			if [[ "$callprogress" =~ ^called-dtmf ]]; then
				dtmfdigit="${callprogress:12}"
				[[ "$dtmfdigit" = "*" ]] && dtmfdigit="star"
				[[ "$dtmfdigit" = "#" ]] && dtmfdigit="pound"
				echo "play filename=/etc/osmocom/sounds/dtmf-${dtmfdigit}.wav"
			fi
			read callprogress
		done
	fi
fi

exit 0
