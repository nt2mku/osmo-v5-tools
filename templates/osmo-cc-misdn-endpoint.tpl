socket ${v5_if}-isdn-${v5_port}
local-tones german
bridging 0
nt
${layer_1_hold}
#debug-misdn
verbose ${verboselevel}
${aocd}
${aocs}
${cc_ringback}
${cnip}
local-tones ${local_tones}
timeouts 60,4,180,180,60
cc 'name v5-${v5_if}-${v5_port}'
cc 'local ${lcl_ip}:${cc_port}'
cc 'remote ${lcl_ip}:4000'
