socket ${v5_if}-pstn-${v5_port}
name ${v5_if}-pstn-${v5_port}
pstn-dialect uk
enblock off
clip pulse
clip-date
verbose ${verboselevel}
${loop_disconnect}
${lr_on_connect}
${recall}
${metering}
local-tones ${local_tones}
cc 'name v5-${v5_if}-${v5_port}'
cc 'local ${lcl_ip}:${cc_port}'
cc 'remote ${lcl_ip}:4000'
