!
! OsmoV5LE (UNKNOWN) configuration saved from vty
!!
!
log stderr
 logging filter all 1
 logging color 1
 logging print category-hex 1
 logging print category 0
 logging print thread-id 0
 logging timestamp 0
 logging print file 1
 logging level v5 notice
 logging level v5l1 notice
 logging level v5ctrl notice
 logging level v5port notice
 logging level v5pstn notice
 logging level v5lcp notice
 logging level v5bcc notice
 logging level v5pp notice
 logging level v5mgmt notice
 logging level v5ef notice
 logging level ph notice
 logging level lglobal notice
 logging level llapd notice
 logging level linp notice
 logging level lmux notice
 logging level lmi notice
 logging level lmib notice
 logging level lsms notice
 logging level lctrl notice
 logging level lgtp notice
 logging level lstats notice
 logging level lgsup notice
 logging level loap notice
 logging level lss7 notice
 logging level lsccp notice
 logging level lsua notice
 logging level lm3ua notice
 logging level lmgcp notice
 logging level ljibuf notice
 logging level lrspro notice
 logging level lns notice
 logging level lbssgp notice
 logging level lnsdata notice
 logging level lnssignal notice
 logging level liuup notice
 logging level lpfcp notice
 logging level lcsn1 notice
 logging level lio notice
!
stats interval 5
!
line vty
 no login
!
e1_input
 e1_line 0 driver e1d
 e1_line 0 port 0
 e1_line 0 name icE1usb
!
interface ${v5_if} v5.2
 auto-restart
 no establish
 pstn-enable early 
 alignment
 no accelerated-alignment
 variant 0
 cc-id 0
 no information-transfer-capability
 link 0
  # primary
  e1 line 0
 ${pstnports}${isdnports}
